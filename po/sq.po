# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: com.ubuntu.calendar\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-03-07 08:29+0000\n"
"PO-Revision-Date: 2018-06-02 20:36+0000\n"
"Last-Translator: Jeff <jeffi27@live.at>\n"
"Language-Team: Albanian <https://translate.ubports.com/projects/ubports/"
"calendar-app/sq/>\n"
"Language: sq\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 2.15\n"

#: ../qml/TimeLineBase.qml:50 ../qml/AllDayEventComponent.qml:89
msgid "New event"
msgstr "Ngjarje e re"

#: ../qml/CalendarChoicePopup.qml:44 ../qml/EventActions.qml:54
msgid "Calendars"
msgstr "Kalendarët"

#: ../qml/CalendarChoicePopup.qml:46 ../qml/SettingsPage.qml:51
msgid "Back"
msgstr "Mbrapa"

#. TRANSLATORS: Please translate this string  to 15 characters only.
#. Currently ,there is no way we can increase width of action menu currently.
#: ../qml/CalendarChoicePopup.qml:58 ../qml/EventActions.qml:39
msgid "Sync"
msgstr "Sinkronizo"

#: ../qml/CalendarChoicePopup.qml:58 ../qml/EventActions.qml:39
msgid "Syncing"
msgstr "Duke Sinkronizuar"

#: ../qml/CalendarChoicePopup.qml:83
msgid "Add online Calendar"
msgstr "Fut kalendar online"

#: ../qml/CalendarChoicePopup.qml:184
msgid "Unable to deselect"
msgstr "Nuk është e mundur të shfuqizohet"

#: ../qml/CalendarChoicePopup.qml:185
msgid ""
"In order to create new events you must have at least one writable calendar "
"selected"
msgstr ""
"Për të krijuar një ngjarje të re, duhet të keni të paktën një kalendar të "
"shkruajtshëm të zgjedhur"

#: ../qml/CalendarChoicePopup.qml:187 ../qml/RemindersPage.qml:80
msgid "Ok"
msgstr "OK"

#. TRANSLATORS: the first argument (%1) refers to a start time for an event,
#. while the second one (%2) refers to the end time
#: ../qml/EventBubble.qml:139
msgid "%1 - %2"
msgstr "%1 - %2"

#: ../qml/RemindersModel.qml:31 ../qml/RemindersModel.qml:99
msgid "No Reminder"
msgstr "Nuk ka memorie"

#. TRANSLATORS: this refers to when a reminder should be shown as a notification
#. in the indicators. "On Event" means that it will be shown right at the time
#. the event starts, not any time before
#: ../qml/RemindersModel.qml:34 ../qml/RemindersModel.qml:103
msgid "On Event"
msgstr "Në Termin"

#: ../qml/RemindersModel.qml:43
msgid "%1 week"
msgid_plural "%1 weeks"
msgstr[0] "%1 jav"
msgstr[1] "%2 javë"

#: ../qml/RemindersModel.qml:54
msgid "%1 day"
msgid_plural "%1 days"
msgstr[0] "%1 dit"
msgstr[1] "%1 ditë"

#: ../qml/RemindersModel.qml:65
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] "%1 orë"
msgstr[1] "%2 orë"

#: ../qml/RemindersModel.qml:74
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minutë"
msgstr[1] "%1 minuta"

#: ../qml/RemindersModel.qml:104
msgid "5 minutes"
msgstr "5 minuta"

#: ../qml/RemindersModel.qml:105
msgid "10 minutes"
msgstr "10 minuta"

#: ../qml/RemindersModel.qml:106
msgid "15 minutes"
msgstr "15 minuta"

#: ../qml/RemindersModel.qml:107
msgid "30 minutes"
msgstr "30 minuta"

#: ../qml/RemindersModel.qml:108
msgid "1 hour"
msgstr "1 orë"

#: ../qml/RemindersModel.qml:109
msgid "2 hours"
msgstr "2 orë"

#: ../qml/RemindersModel.qml:110
msgid "1 day"
msgstr "1 ditë"

#: ../qml/RemindersModel.qml:111
msgid "2 days"
msgstr "2 ditë"

#: ../qml/RemindersModel.qml:112
msgid "1 week"
msgstr "1 javë"

#: ../qml/RemindersModel.qml:113
msgid "2 weeks"
msgstr "2 javë"

#: ../qml/RemindersModel.qml:114
msgid "Custom"
msgstr "E përsonalizuar"

#: ../qml/AgendaView.qml:50 ../qml/calendar.qml:333 ../qml/calendar.qml:514
msgid "Agenda"
msgstr "Terminet"

#: ../qml/AgendaView.qml:95
msgid "You have no calendars enabled"
msgstr "Nuk keni aktivizuar kalendarë"

#: ../qml/AgendaView.qml:95
msgid "No upcoming events"
msgstr "Nuk ka termine të ardhshme"

#: ../qml/AgendaView.qml:107
msgid "Enable calendars"
msgstr "Aktivizo kalendarin"

#: ../qml/AgendaView.qml:199
msgid "no event name set"
msgstr "asnjë emër ngjarjeje"

#: ../qml/AgendaView.qml:201
msgid "no location"
msgstr "Asnjë vend"

#: ../qml/LimitLabelModel.qml:25
msgid "Never"
msgstr "Kurrë"

#: ../qml/LimitLabelModel.qml:26
msgid "After X Occurrence"
msgstr "Pas X herë"

#: ../qml/LimitLabelModel.qml:27
msgid "After Date"
msgstr "Pas datës"

#: ../qml/NewEventBottomEdge.qml:54 ../qml/NewEvent.qml:382
msgid "New Event"
msgstr "Ngjarje e re"

#. TRANSLATORS: Please keep the translation of this string to a max of
#. 5 characters as the week view where it is shown has limited space.
#: ../qml/AllDayEventComponent.qml:148
msgid "%1 event"
msgid_plural "%1 events"
msgstr[0] "% 1 Ngjarje"
msgstr[1] "%1 Ngjarje"

#. TRANSLATORS: the argument refers to the number of all day events
#: ../qml/AllDayEventComponent.qml:152
msgid "%1 all day event"
msgid_plural "%1 all day events"
msgstr[0] "% 1 takim gjithë-ditor"
msgstr[1] "% 1 takime gjithë-ditëshe"

#: ../qml/EditEventConfirmationDialog.qml:29 ../qml/NewEvent.qml:382
msgid "Edit Event"
msgstr "Ndrysho ngjarjen"

#. TRANSLATORS: argument (%1) refers to an event name.
#: ../qml/EditEventConfirmationDialog.qml:32
msgid "Edit only this event \"%1\", or all events in the series?"
msgstr "Vetëm modifikoni këtë termin \"% 1\" ose të gjitha emërimet në seri?"

#: ../qml/EditEventConfirmationDialog.qml:35
msgid "Edit series"
msgstr "Redakto serinë"

#: ../qml/EditEventConfirmationDialog.qml:44
msgid "Edit this"
msgstr "Redakto këtë"

#: ../qml/EditEventConfirmationDialog.qml:53
#: ../qml/DeleteConfirmationDialog.qml:60 ../qml/RemindersPage.qml:72
#: ../qml/NewEvent.qml:387 ../qml/OnlineAccountsHelper.qml:73
#: ../qml/ColorPickerDialog.qml:55
msgid "Cancel"
msgstr "Anulo"

#: ../qml/ContactChoicePopup.qml:37
msgid "No contact"
msgstr "Asnjë kontakt"

#: ../qml/ContactChoicePopup.qml:96
msgid "Search contact"
msgstr "Kërko kontaktin"

#: ../qml/DeleteConfirmationDialog.qml:31
msgid "Delete Recurring Event"
msgstr "Fshije termini e përsëritur"

#: ../qml/DeleteConfirmationDialog.qml:32
msgid "Delete Event"
msgstr "Fshijë terminin"

#. TRANSLATORS: argument (%1) refers to an event name.
#: ../qml/DeleteConfirmationDialog.qml:36
msgid "Delete only this event \"%1\", or all events in the series?"
msgstr "Fshini vetem këtë ngjarje »%1« ose të gjitha ngjarjet në seri?"

#: ../qml/DeleteConfirmationDialog.qml:37
msgid "Are you sure you want to delete the event \"%1\"?"
msgstr "Jeni i sigurt që doni të fshini terminin »%1«?"

#: ../qml/DeleteConfirmationDialog.qml:40
msgid "Delete series"
msgstr "Fshini serinë"

#: ../qml/DeleteConfirmationDialog.qml:51
msgid "Delete this"
msgstr "Fshije këtë"

#: ../qml/DeleteConfirmationDialog.qml:51 ../qml/NewEvent.qml:394
msgid "Delete"
msgstr "Fshijë"

#: ../qml/calendar.qml:74
msgid ""
"Calendar app accept four arguments: --starttime, --endtime, --newevent and --"
"eventid. They will be managed by system. See the source for a full comment "
"about them"
msgstr ""
"Aplikacioni kalendarik pranon katër argumente: - koha e fillimit, - koha e "
"pritjes, - e reja dhe - e ardhshme. Këto menaxhohen nga sistemi. Shikoni "
"kodin burimor për të marrë një përshkrim të plotë"

#: ../qml/calendar.qml:341 ../qml/calendar.qml:535
msgid "Day"
msgstr "Ditë"

#: ../qml/calendar.qml:349 ../qml/calendar.qml:556
msgid "Week"
msgstr "Java"

#: ../qml/calendar.qml:357 ../qml/calendar.qml:577
msgid "Month"
msgstr "Muaji"

#: ../qml/calendar.qml:365 ../qml/calendar.qml:598
msgid "Year"
msgstr "Viti"

#: ../qml/calendar.qml:705 ../qml/TimeLineHeader.qml:66
#: ../qml/EventDetails.qml:173
msgid "All Day"
msgstr "Ditorë"

#: ../qml/SettingsPage.qml:49 ../qml/EventActions.qml:66
msgid "Settings"
msgstr "Cilsimet"

#: ../qml/SettingsPage.qml:72
msgid "Show week numbers"
msgstr "Shfaq javën e kalendarit"

#: ../qml/SettingsPage.qml:90
msgid "Display Chinese calendar"
msgstr ""

#: ../qml/SettingsPage.qml:110
msgid "Business hours"
msgstr ""

#: ../qml/SettingsPage.qml:211
msgid "Default reminder"
msgstr "Kujtesës standarde"

#: ../qml/SettingsPage.qml:255
msgid "Default calendar"
msgstr "Kalendari i paracaktuar"

#: ../qml/RemindersPage.qml:62
msgid "Custom reminder"
msgstr "Kujtues personal"

#. TRANSLATORS: This is shown in the month view as "Wk" as a title
#. to indicate the week numbers. It should be a max of up to 3 characters.
#: ../qml/MonthComponent.qml:293
msgid "Wk"
msgstr "KJ"

#. TRANSLATORS: this refers to how often a recurrent event repeats
#. and it is shown as the header of the page to choose repetition
#. and as the header of the list item that shows the repetition
#. summary in the page that displays the event details
#: ../qml/EventRepetition.qml:40 ../qml/EventRepetition.qml:167
msgid "Repeat"
msgstr "Përserit"

#: ../qml/EventRepetition.qml:187
msgid "Repeats On:"
msgstr "Përsërit:"

#: ../qml/EventRepetition.qml:233
#, fuzzy
msgid "Interval of recurrence"
msgstr "Pas X herë"

#: ../qml/EventRepetition.qml:258
msgid "Recurring event ends"
msgstr "Takimi i përsëriur mbaron"

#. TRANSLATORS: this refers to how often a recurrent event repeats
#. and it is shown as the header of the option selector to choose
#. its repetition
#: ../qml/EventRepetition.qml:282 ../qml/NewEvent.qml:775
msgid "Repeats"
msgstr "Përsëritjet"

#: ../qml/EventRepetition.qml:308
msgid "Date"
msgstr "Data"

#: ../qml/YearView.qml:57 ../qml/WeekView.qml:60 ../qml/MonthView.qml:50
#: ../qml/DayView.qml:76
msgid "Today"
msgstr "Sot"

#: ../qml/YearView.qml:79
msgid "Year %1"
msgstr "Viti %1"

#: ../qml/NewEvent.qml:199
msgid "End time can't be before start time"
msgstr "Koha e përfundimit nuk mund të jetë para fillimit të kohës"

#: ../qml/NewEvent.qml:412
msgid "Save"
msgstr "Ruaj"

#: ../qml/NewEvent.qml:423
msgid "Error"
msgstr "Gabim"

#: ../qml/NewEvent.qml:425
msgid "OK"
msgstr "OK"

#: ../qml/NewEvent.qml:487
msgid "From"
msgstr "Nga"

#: ../qml/NewEvent.qml:503
msgid "To"
msgstr "Marrësi"

#: ../qml/NewEvent.qml:530
msgid "All day event"
msgstr "Takim gjithë-ditor"

#: ../qml/NewEvent.qml:553 ../qml/EventDetails.qml:37
msgid "Event Details"
msgstr "Detajet e ngjarjes"

#: ../qml/NewEvent.qml:567
msgid "Event Name"
msgstr "Emri i ngjarjes"

#: ../qml/NewEvent.qml:585 ../qml/EventDetails.qml:437
msgid "Description"
msgstr "Përshkrimi"

#: ../qml/NewEvent.qml:604
msgid "Location"
msgstr "Vendndodhja"

#: ../qml/NewEvent.qml:619 ../qml/EventDetails.qml:348
#: com.ubuntu.calendar_calendar.desktop.in.h:1
msgid "Calendar"
msgstr "Kalendari"

#: ../qml/NewEvent.qml:681
msgid "Guests"
msgstr "Mysafirët"

#: ../qml/NewEvent.qml:691
msgid "Add Guest"
msgstr "Fut mysafirë"

#: ../qml/NewEvent.qml:797 ../qml/NewEvent.qml:814 ../qml/EventDetails.qml:464
msgid "Reminder"
msgstr "Kujtesë"

#: ../qml/WeekView.qml:137 ../qml/MonthView.qml:76
msgid "%1 %2"
msgstr "%1 %2"

#: ../qml/WeekView.qml:144 ../qml/WeekView.qml:145
msgid "MMM"
msgstr "MMM"

#. TRANSLATORS: this is a time formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#. It's used in the header of the month and week views
#: ../qml/WeekView.qml:156 ../qml/MonthView.qml:81 ../qml/DayView.qml:126
msgid "MMMM yyyy"
msgstr "MMMM yyyy"

#. TRANSLATORS: W refers to Week, followed by the actual week number (%1)
#: ../qml/TimeLineHeader.qml:54
msgid "W%1"
msgstr "J%1"

#: ../qml/EventDetails.qml:40
msgid "Edit"
msgstr "Ndrysho"

#: ../qml/EventDetails.qml:392
msgid "Attending"
msgstr "Të pranishëm"

#: ../qml/EventDetails.qml:394
msgid "Not Attending"
msgstr "Jo pjesëmarrës"

#: ../qml/EventDetails.qml:396
msgid "Maybe"
msgstr "Ndoshta"

#: ../qml/EventDetails.qml:398
msgid "No Reply"
msgstr "Nuk  përgjigjet"

#: ../qml/OnlineAccountsHelper.qml:39
msgid "Pick an account to create."
msgstr "Zgjidh një llogari për të krijuar."

#: ../qml/ColorPickerDialog.qml:25
msgid "Select Color"
msgstr "Zgjidhë ngjyren"

#: ../qml/RecurrenceLabelDefines.qml:23
msgid "Once"
msgstr "Një herë"

#: ../qml/RecurrenceLabelDefines.qml:24
msgid "Daily"
msgstr "Ditorë"

#: ../qml/RecurrenceLabelDefines.qml:25
msgid "On Weekdays"
msgstr "Në ditët e punës"

#. TRANSLATORS: The arguments refer to days of the week. E.g. "On Monday, Tuesday, Thursday"
#: ../qml/RecurrenceLabelDefines.qml:27
msgid "On %1, %2 ,%3"
msgstr "Në %1, %2, %3"

#. TRANSLATORS: The arguments refer to days of the week. E.g. "On Monday and Thursday"
#: ../qml/RecurrenceLabelDefines.qml:29
msgid "On %1 and %2"
msgstr "Në %1 dhe %2"

#: ../qml/RecurrenceLabelDefines.qml:30
msgid "Weekly"
msgstr "Javor"

#: ../qml/RecurrenceLabelDefines.qml:31
msgid "Monthly"
msgstr "Mujor"

#: ../qml/RecurrenceLabelDefines.qml:32
msgid "Yearly"
msgstr "Çdo vit"

#. TRANSLATORS: the argument refers to multiple recurrence of event with count .
#. E.g. "Daily; 5 times."
#: ../qml/EventUtils.qml:75
msgid "%1; %2 time"
msgid_plural "%1; %2 times"
msgstr[0] "%1; %2 her"
msgstr[1] "%1; %2 herë"

#. TRANSLATORS: the argument refers to recurrence until user selected date.
#. E.g. "Daily; until 12/12/2014."
#: ../qml/EventUtils.qml:79
msgid "%1; until %2"
msgstr "%1; bis %2"

#: ../qml/EventUtils.qml:93
msgid "; every %1 days"
msgstr ""

#: ../qml/EventUtils.qml:95
#, fuzzy
msgid "; every %1 weeks"
msgstr "%1 jav"

#: ../qml/EventUtils.qml:97
msgid "; every %1 months"
msgstr ""

#: ../qml/EventUtils.qml:99
msgid "; every %1 years"
msgstr ""

#. TRANSLATORS: the argument refers to several different days of the week.
#. E.g. "Weekly on Mondays, Tuesdays"
#: ../qml/EventUtils.qml:125
msgid "Weekly on %1"
msgstr "Javore në% 1"

#: com.ubuntu.calendar_calendar.desktop.in.h:2
msgid "A calendar for Ubuntu which syncs with online accounts."
msgstr "Një kalendar për Ubuntu, i cili sinkronizohet me Internetkonten."

#: com.ubuntu.calendar_calendar.desktop.in.h:3
msgid "calendar;event;day;week;year;appointment;meeting;"
msgstr "Kalendar; ngjarje; ditë; javë; vit; termin; takim;"

#~ msgid "Show lunar calendar"
#~ msgstr "Shiko kalendarin e hënës"
